const os = require('os');

console.log(os.platform()); // платформа
console.log(os.arch()); // архитектура
console.log(os.cpus()); // Общая информация о ЦПУ

console.log(os.freemem()); //Свободная память
console.log(os.totalmem()); //Всего памяти

console.log(os.homedir()); //Корневая директория на компьютере

console.log(os.uptime()); //Сколько система находится в работе (в миллисекундах)