const path = require("path");

console.log(path.dirname(__filename));
console.log(path.extname(__filename));
console.log(path.parse(__filename));

const pathReq = path.join(__dirname, 'test', 'second.html');
const pathReq2 = path.resolve(__dirname, '/test', 'second.html');

console.log('pathReq', pathReq);
console.log('pathReq2', pathReq2);