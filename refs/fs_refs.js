const fs = require('fs'); // fs - File System
const path = require('path');

const createDir = (dirName, successMsg) => fs.mkdir(path.join(__dirname, dirName), err => {
    if (err) throw err;

    console.log(successMsg);
}); // - создание папки

const writeFile = (dirName, fileName, contentFile, msgSuccess) => fs.writeFile(
    path.join(__dirname, dirName, fileName),
    contentFile,
    (err) => {
        if (err) throw err;
        console.log(msgSuccess);
    }
); // создание файла

const appendFile = (dirName, fileName, appendContent, succeessMessage) =>
    fs.appendFile(
        path.resolve(__dirname, dirName, fileName),
        appendContent,
        err => {
            if(err) throw err;
            console.log(succeessMessage);
        }
    ) // добавление в конец файла контента


const readFile = (dirName, fileName, callback) => fs.readFile(
    path.join(__dirname, dirName, dirName),
    (err, data) => {
        if(err) throw err;
        callback(data);
    }
); // чтение файла


const renameFile = (oldPath, newPath, msgSuccess) => fs.rename(
    oldPath,
    newPath,
    err => {
        if(err) throw err;
        console.log(msgSuccess);
    }
); // - позволяет изменить путь файла и имя файла. (меняет текущий, не создает новый).




